package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
public class Fridge implements IFridge { private int size, items; private List<FridgeItem> container;
    public Fridge()
    {
        size=20;
        items=0;
        container = new ArrayList<>();
    }
    public Fridge(List<FridgeItem> list)
    {
        this();
        if(list!=null && list.size() <= size)
        {
         container=list;
         items=list.size();
        }
    }
    @Override public int nItemsInFridge()
    {
        return items;
    }
    @Override public int totalSize()
    {
        return size-items; // return total space
    }
    @Override public boolean placeIn(FridgeItem item)
    {
        if(totalSize()>0)
        {
            container.add(item);
            items++;
            return true;
        }
        else
        {
            return false; // not added to list
        }
    }
    @Override public void takeOut(FridgeItem item)
    {
        if(container.contains(item))
        {
            container.remove(item);
            items--;
        }
        else
        {
            throw new NoSuchElementException();
        }
    }
    @Override public void emptyFridge()
    {
        container.clear();
        items=0;
    }
    @Override public List<FridgeItem> removeExpiredFood()
    {
        List<FridgeItem> expired = new ArrayList<>();
        for(int i=0;i<container.size();i++)
        {
            FridgeItem item=container.get(i);
            if(item.hasExpired())
            {
                container.remove(i);
                items--;
                i--;

                expired.add(item);
            }
        } return expired;
    }
}
